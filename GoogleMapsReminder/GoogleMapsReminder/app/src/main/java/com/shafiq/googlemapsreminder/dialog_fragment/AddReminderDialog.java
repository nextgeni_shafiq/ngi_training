package com.shafiq.googlemapsreminder.dialog_fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.shafiq.googlemapsreminder.R;

public class AddReminderDialog extends AppCompatDialogFragment {

    private AddReminderDialogListener mAddReminderDialogListener;
    private EditText mTitleEditText;
    private EditText mDescriptionEditText;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog, null);

        mTitleEditText = view.findViewById(R.id.et_title);
        mDescriptionEditText = view.findViewById(R.id.et_description);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setTitle("Enter your Reminder !")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = mTitleEditText.getText().toString();
                        String description = mDescriptionEditText.getText().toString();
                        mAddReminderDialogListener.applyText(title, description);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Nothing goes here
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            mAddReminderDialogListener = (AddReminderDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddReminderDialogListener");
        }
    }

    public interface AddReminderDialogListener {
        void applyText(String title, String description);
    }
}
